public class Card {


    private String suit;
    private String face;


    public Card(String suit, String face){
        this.suit = suit;
        this.face = face;
    }

    public boolean equals(Card card){
        return card.getSuit().equals(this.getSuit()) && card.getFace().equals(this.getFace());
    }


    public String getFace() {
        return face;
    }

    public String getSuit() {
        return suit;
    }
}
