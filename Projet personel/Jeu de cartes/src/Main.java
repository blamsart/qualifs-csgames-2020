import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {

        ArrayList<Card> deck = new ArrayList<Card>();
        ArrayList<Card> fullDeck = Randomizer.generateFullDeck();
        ArrayList<Card> pickDeck = new ArrayList<Card>();


        Scanner sc = new Scanner(System.in);

        boolean redo = false;
        String name = "";

        do {
            try {
                redo = false;

                System.out.println("Member name");
                name = sc.nextLine();

                LectureJSON dataLog = new LectureJSON(".\\json\\" + name + ".json");

                fillDeck(dataLog, deck);

            } catch (IOException ioe) {
                System.out.println("\nI couldn't find this name. Is this a new player?(Y/N)");
                String answer = sc.nextLine();
                if (answer.toUpperCase().equals("N")) {
                    redo = true;
                } else if (answer.toUpperCase().equals("Y")){
                    System.out.println("\nPlayer added!\n");
                }
            }
        } while (redo);


        if (deck.size() == 54) {
            System.out.println("The deck is full");
        } else {

            if (deck.size() == 0){
                pickDeck = fullDeck;
            } else {
                for (Card card : fullDeck) {
                    for (int i = 0; i < deck.size(); i++) {
                        if (deck.get(i).equals(card)) {
                            i = deck.size();
                        } else if (i == deck.size() - 1) {
                            pickDeck.add(card);
                        }
                    }
                }
            }
            Card randomCard = Randomizer.generateCard(pickDeck);


            if (randomCard.getFace().equals("joker")) {
                System.out.println(randomCard.getSuit() + " " + randomCard.getFace());
            } else {
                System.out.println(randomCard.getFace() + " of " + randomCard.getSuit());
            }


            System.out.println("\nDo you want to add the card? Y/N");
            if (sc.nextLine().equals("y") || sc.nextLine().equals("Y")) {
                deck.add(randomCard);
                System.out.println("\nAdded!");
            }
            System.out.println("The deck has " + deck.size() + " cards in it.");

            try {
                LectureJSON.addCard(".\\json\\" + name + ".json", deck);
            } catch (IOException ioe) {
                System.out.println("ERROR");
            }

        }
    }

    public static void fillDeck(LectureJSON dataLog, ArrayList<Card> deck) {
        addHearts(dataLog, deck);
        addClubs(dataLog, deck);
        addDiamonds(dataLog, deck);
        addSpades(dataLog, deck);
        addJokers(dataLog, deck);
    }

    public static void addHearts(LectureJSON dataLog, ArrayList<Card> deck) {
        for (int i = 0; i < dataLog.getNbHearts(); i++) {
            deck.add(new Card("hearts", dataLog.getHeart(i)));
        }
    }

    public static void addClubs(LectureJSON dataLog, ArrayList<Card> deck) {
        for (int i = 0; i < dataLog.getNbClubs(); i++) {
            deck.add(new Card("clubs", dataLog.getClub(i)));
        }
    }

    public static void addSpades(LectureJSON dataLog, ArrayList<Card> deck) {
        for (int i = 0; i < dataLog.getNbSpades(); i++) {
            deck.add(new Card("spades", dataLog.getSpade(i)));
        }
    }

    public static void addDiamonds(LectureJSON dataLog, ArrayList<Card> deck) {
        for (int i = 0; i < dataLog.getNbDiamonds(); i++) {
            deck.add(new Card("diamonds", dataLog.getDiamond(i)));
        }
    }

    public static void addJokers(LectureJSON dataLog, ArrayList<Card> deck) {
        for (int i = 0; i < dataLog.getNbJokers(); i++) {
            deck.add(new Card(dataLog.getJoker(i), "joker"));
        }
    }


}
