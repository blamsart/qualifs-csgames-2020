import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Randomizer {

    final static ArrayList<String> allCards = new ArrayList<String>(Arrays.asList("A", "2", "3", "4", "5", "6", "7"
                                                                            , "8", "9", "10", "J", "Q", "K", "joker"));
    final static ArrayList<String> jokerCards = new ArrayList<String>(Arrays.asList("Black", "Red"));
    final static ArrayList<String> suits = new ArrayList<String>(Arrays.asList("hearts", "clubs", "diamonds", "spades"));

    public static Card generateCard(ArrayList<Card> deck){
        Random r = new Random();
        return deck.get(r.nextInt(deck.size()));
    }

    public static ArrayList<Card> generateFullDeck(){
        ArrayList<Card> fullDeck = new ArrayList<Card>();

        for (String face : allCards) {
            if (face.equals("joker")) {
                for (String suit : jokerCards) {
                    fullDeck.add(new Card(suit, face));
                }
            } else {
                for (String suit : suits) {
                    fullDeck.add(new Card(suit, face));
                }
            }
        }
        return fullDeck;
    }



}
