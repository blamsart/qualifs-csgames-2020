import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class LectureJSON {

    // Le chemin vers le fichier JSON
    private static JSONObject fichierJSON;

    // Liste des cartes
    private static JSONArray arrayHearts;
    private static JSONArray arraySpades;
    private static JSONArray arrayClubs;
    private static JSONArray arrayDiamonds;
    private static JSONArray arrayJokers;

    /**
     * Constucteur de LectureJSON
     *
     * @param cheminFichier Le chemin vers le fichier JSON
     * @throws IOException Exception pour l'entree ou sortie de donnee
     */
    protected LectureJSON(String cheminFichier) throws IOException {
        fichierJSON = (JSONObject) JSONSerializer.toJSON(IOUtils.toString(new FileInputStream(cheminFichier), "UTF-8"));
        arrayHearts = fichierJSON.getJSONArray("hearts");
        arrayClubs = fichierJSON.getJSONArray("clubs");
        arrayDiamonds = fichierJSON.getJSONArray("diamonds");
        arraySpades = fichierJSON.getJSONArray("spades");
        arrayJokers = fichierJSON.getJSONArray("joker");
    }


    /**
     * Permet de générer un fichier JSON avec les résultats de la validation.
     *
     * @param cheminFichier Le chemin vers le fichier JSON
     * @param deck  La liste des messages d'erreur
     * @throws IOException Exception pour l'entrée ou sortie de donnée
     */
    static void addCard(final String cheminFichier, final ArrayList<Card> deck)
            throws IOException {

        final File fichier = new File(cheminFichier);
        ArrayList<String> hearts = new ArrayList<String>();
        ArrayList<String> clubs = new ArrayList<String>();
        ArrayList<String> diamonds = new ArrayList<String>();
        ArrayList<String> spades = new ArrayList<String>();
        ArrayList<String> jokers = new ArrayList<String>();
        final JSONObject output = new JSONObject();

        for (Card card : deck) {
            if (card.getSuit().equals("hearts")) {
                hearts.add(card.getFace());
            } else if (card.getSuit().equals("clubs")) {
                clubs.add(card.getFace());
            } else if (card.getSuit().equals("diamonds")) {
                diamonds.add(card.getFace());
            } else if (card.getSuit().equals("spades")) {
                spades.add(card.getFace());
            } else if (card.getFace().equals("joker")) {
                jokers.add(card.getSuit());
            }
        }

        output.put("hearts", hearts);
        output.put("clubs", clubs);
        output.put("diamonds", diamonds);
        output.put("spades", spades);
        output.put("joker", jokers);

        FileUtils.writeStringToFile(fichier, output.toString(4), "UTF-8");
    }




    /**
     * @param indice Emplacement
     * @return carte
     */
    protected String getHeart(int indice) {
        return arrayHearts.getString(indice);
    }

    /**
     * @param indice Emplacement
     * @return carte
     */
    protected String getClub(int indice) {
        return arrayClubs.getString(indice);
    }

    /**
     * @param indice Emplacement
     * @return carte
     */
    protected String getSpade(int indice) {
        return arraySpades.getString(indice);
    }

    /**
     * @param indice Emplacement
     * @return carte
     */
    protected String getDiamond(int indice) {
        return arrayDiamonds.getString(indice);
    }

    /**
     * @param indice Emplacement
     * @return carte
     */
    protected String getJoker(int indice) {
        return arrayJokers.getString(indice);
    }

    // QUANTITÉ //

    /**
     * @return le nombre de cartes coeurs
     */
    protected int getNbHearts() {
        return arrayHearts.size();
    }

    /**
     * @return le nombre de cartes clubs
     */
    protected int getNbClubs() {
        return arrayClubs.size();
    }

    /**
     * @return le nombre de cartes spades
     */
    protected int getNbSpades() {
        return arraySpades.size();
    }

    /**
     * @return le nombre de cartes diamonds
     */
    protected int getNbDiamonds() {
        return arrayDiamonds.size();
    }

    /**
     * @return le nombre de cartes jokers
     */
    protected int getNbJokers() {
        return arrayJokers.size();
    }

}

